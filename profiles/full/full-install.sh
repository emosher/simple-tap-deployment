#!/bin/bash

set -e
set -u
set -o pipefail

TAP_VERSION=$(yq -r .tap.version tap-install-config.yml)
TBS_VERSION=$(yq -r .tap.tbs_deps_version tap-install-config.yml)

# TODO: would like to standardize this to either kubectl or kapp commands, I don't care which

# Install pre-reqs
echo -e "\nInstalling pre-reqs...\n"
ytt --data-values-file tap-install-config.yml --data-values-file tap-install-secrets.yml -f ../../prereqs | kubectl apply -f-

# Create the values file from templated configs
echo -e "\nGenerating TAP Values file...\n"
ytt --data-values-file tap-install-config.yml --data-values-file tap-install-secrets.yml -f tap-values-templated.yml > tap-values-full.yml

# Install TAP and/or update it if changed
echo -e "\nInstalling TAP from values file...\n"
tanzu package installed update tap -p tap.tanzu.vmware.com -v $TAP_VERSION  --values-file tap-values-full.yml -n tap-install --poll-interval 5s --install

# Install full TBS dependencies
echo -e "\nInstalling full TBS dependencies...\n"
tanzu package installed update full-tbs-deps -p full-tbs-deps.tanzu.vmware.com -v $TBS_VERSION -n tap-install --poll-interval 5s --install

# kctrl package installed kick -i full-tbs-deps -n tap-install -y

echo -e "\nConfiguring External DNS and Certificates...\n"
# Creating the external DNS bits
ytt --data-values-file tap-install-config.yml --data-values-file tap-install-secrets.yml -f ../../additional/certificates -f ../../additional/external-dns > tap-external-dns.yml

kubectl apply -f tap-external-dns.yml

# echo -e "\nConfiguring Developer Namespaces...\n"
# Deploy dev namespace configs
ytt --data-values-file tap-install-secrets.yml --data-values-file tap-install-config.yml -f ../../additional/dev-namespaces > tap-dev-namespaces.yml

kubectl apply -f tap-dev-namespaces.yml
