# Simple Tanzu App Platform (TAP) Deployment

## Goal
This repo helps you deploy TAP onto your Kubernetes cluster.

This repo is only concerned with helping you generate the necessary k8s manifest files via (simple!) `ytt` templating. Once you have created the manifests, the way you deploy them is up to you (i.e. `kubectl`, `kapp`, `helm`, etc). It has been built in such a way to allow you to make changes/additions to your workflow with minimal effort/learning. Please feel free to fork this repo and make it your own.

## How Does it Work?
This repo supports running multiple "profiles" for TAP. In the `profiles` directory, you will find a directory for each profile (i.e. "full" or "run"). Inside each of those directories, all your configuration information will be stored in two files:

- [tap-install-config.yml](tap-install-config.yml)
- [tap-install-secrets.yml](tap-install-secrets.yml.example)

To create your `tap-install-secrets.yml`, copy `tap-install-secrets.yml.example` from the root of this repo over to `tap-install-secrets.yml` in the appropriate profile directory and DO NOT commit it.

When generating manifest files for deployment, you will use the following command format:

```bash
ytt \
--data-values-file tap-install-config.yml \
--data-values-file tap-install-secrets.yml \
-f <templated-file-you-care-about> \
-f <another-templated-file-you-care-about>
```

This will result in a k8s manifest file being printed to the screen. From there, you can redirect to `kubectl`, `kapp`, or your favorite k8s deployment utility.

Building on this example, to simply use `kubectl` to deploy, you would use the following command:

```bash
ytt \
--data-values-file tap-install-config.yml \
--data-values-file tap-install-secrets.yml \
-f <templated-file-you-care-about> \
-f <another-templated-file-you-care-about> | kubectl apply -f-
```

We recommend this "redirect to a deployment tool" method instead of redirecting to a file. This is because the contents of your manifests will change with regularity. And you don't want to manage the manifest files, you want to manage _the data_ that influences those manifest files. 

TL;DR - Don't commit manifest files to Git. Just re-run the `ytt` command to regenerate them, then apply them.

## Assumptions / Prereqs

### Cluster Essentials
This repo assumes you already have `cluster-essentials` installed on your k8s cluster. 

### Container Registry
This repo requires that you host your own TAP container images in your own registry.

Consult the docs for the latest information, but you should be able to pivot the TAP images to your registry with the following command:

```bash
imgpkg copy -b registry.tanzu.vmware.com/tanzu-application-platform/tap-packages:{TAP_VERSION} --to-repo {REGISTRY_HOSTNAME}/{REPO_NAME}/{PROJECT_NAME}
```

As an example:
```bash
imgpkg copy -b registry.tanzu.vmware.com/tanzu-application-platform/tap-packages:1.2.0-build.18 --to-repo harbor.tanzu.tacticalprogramming.com/tap-1.2.0-build.18/tap-packages
```

To fetch list of all available versions of TAP, run the following command.
```
imgpkg tag list -i registry.tanzu.vmware.com/tanzu-application-platform/tap-packages | grep -v sha | sort -V
```

NOTE: You will need to create the `{REPO_NAME}` in your registry ahead of time. `imgpkg` will not create the repo for you.

### [Optional] External DNS
If you want to have `external-dns` manage the DNS entries for you (for example, routes to your apps and to tap-gui), use the [external-dns](additional/external-dns/external-dns.yml) manifest provided. 

Like all files, this file is ytt-templated. To generate the full manifest, run the following command (assuming you are in one of your profile directories):
```bash
ytt \
--data-values-file tap-install-config.yml \
--data-values-file tap-install-secrets.yml \
-f ../../additional/external-dns/external-dns.yml
```

NOTE: This file is configured to support Route53 and CloudFlare. You will need to modify it (and the two `data-values-file` files above) to provide support for other DNS providers.

## Install

### Configure

You will need to fill out your two files:
- [tap-install-config.yml](tap-install-config.yml)
- [tap-install-secrets.yml](tap-install-secrets.yml.example)

Copy `tap-install-secrets.yml.example` over to `tap-install-secrets.yml` and DO NOT commit it.


### Prereqs

Before deploying TAP, we need to:
- configure a PackageRepository
- create a k8s namespace for TAP
- configure a ServiceAccount and RoleBinding
- create a secret to allow TAP to work with our registry

Everything needed for this is supplied in the [prereqs](prereqs) directory.

Once you've filled out the two configuration files (explained in the 'Configure' section above), run the below command to (for example) generate the manifest file for the TAP Namespace:

```bash
$ pwd
~/simple-tap-deployment/profiles/full

ytt \
--data-values-file tap-install-config.yml \
--data-values-file tap-install-secrets.yml \
-f ../../prereqs/tap-namespace.yml
```

As explained in the top of this README, you will use the resulting k8s manifest to deploy via your favorite method.

For example:
```bash
ytt \
--data-values-file tap-install-config.yml \
--data-values-file tap-install-secrets.yml \
-f ../../prereqs/tap-namespace.yml | kubectl apply -f-
```

Do this for each manifest in the `prereqs` directory.

### Deploy

Again, we will use the two configuration files to create our `tap-values.yml`.
```bash
ytt \
--data-values-file tap-install-config.yml \
--data-values-file tap-install-secrets.yml \
-f tap-values-templated
```

Confirm the values are correct, redirect the output to a file, and you now have the `tap-values.yml` that you need to provide to the `tanzu package install....` command.
```bash
ytt \
--data-values-file tap-install-config.yml \
--data-values-file tap-install-secrets.yml \
-f tap-values-templated.yml > tap-values.yml
```

NOTE: This is _THE ONLY EXCEPTION_ to our recommendation against redirecting to a file. In this very specific case, we redirect to a file because it simplifies the `tanzu` commands necessary to deploy TAP.

NOTE: Because it contains secrets, _DO NOT_ commit `tap-values.yml` to git. 

To deploy:

```bash
tanzu package install tap \
-p tap.tanzu.vmware.com \
-v {TAP_VERSION} \
--values-file tap-values-full.yml \
-n tap-install \
--poll-interval 5s
```

As an example:
```bash
tanzu package install tap \
-p tap.tanzu.vmware.com \
-v 1.3.0 \
--values-file tap-values-full.yml \
-n tap-install \
--poll-interval 5s
```

## Full TBS Dependencies

Refer to [this section](https://docs.vmware.com/en/VMware-Tanzu-Application-Platform/1.3/tap/GUID-install.html#install-the-full-dependencies-package-7) of the docs.  The Package Repository is set up for you in the pre-reqs.  You will need to move the TBS deps to your own repo with something like the following.

```bash
imgpkg copy -b registry.tanzu.vmware.com/tanzu-application-platform/full-tbs-deps-package-repo:VERSION \
  --to-repo ${INSTALL_REGISTRY_HOSTNAME}/${INSTALL_REPO}/tbs-full-deps
```

Then you can install the full TBS deps with the tanzu CLI, such as:
```bash
tanzu package install full-tbs-deps -p full-tbs-deps.tanzu.vmware.com -v VERSION -n tap-install
```

## Additional Features

In addition to simply deploying TAP, the [additional](additional) directory has follow-on features, such as setting up developer namespaces and Tekton pipelines (for running workshops), enabling the CVE view in the tap-gui, and additional Supply Chains for a richer experience. 

All of these "additional" features follow the same deployment process. They pull information from the same two files (`tap-install-configs.yml` and `tap-install-secrets.yml`) with a `ytt` command and generate your manifest file.


## Uninstalling

In the event you need to remove TAP, it's not as simple as just `tanzu package installed delete....`. Doing so will orphan various CRDs (like `workloads`, `scanningPolicies`, and others), which will never delete because the k8s API is no longer aware of those CRDs and/or the service account associated with some objects will have been deleted.

In short, you should delete all k8s objects/CRDs associated with TAP before uninstalling. Consider:

- `workloads`
- [developer namespaces](additional/dev-namespaces/), and any [Grype or other scan tool objects](additional/set-up-scanning-testing/) deployed into them

Once you're confident in your cleanup, simply delete with:
```bash
tanzu package installed delete tap -n tap-install
```